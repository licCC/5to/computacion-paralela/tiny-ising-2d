# Compilacion

`bash runs/antonio.sh 0.0 5 runs/banderas.txt`
donde
- El primer argumento del script identifica la version del codigo
- El segundo la cantidad de veces que se quiere ejecutar cada compilacion
- El tercero el archivo con los conjuntos de banderas de compilacion

# Entrega Lab1

## Playground
A lo largo del primer laboratorio variamos 1) con qué conjunto de banderas compilamos el proyecto, 2) con qué cantidad de sitios (L), 3) en qué máquina corrimos la simulación y 4) parte del código.

1) Las combinaciones de banderas más relevantes están listadas en runs/banderas.txt.

2) Los tamaños de malla que usamos son: 24, 48, 64, 96, 128, 256, 384, 512 y 1024.

3) Las máquinas que usamos son las personales y las provistas por la famaf via conexión ssh: atom y zx81. Ordenadas según poder de cómputo atom > zx81 > piazzolla > delfi.

4) Probamos evitar inicializar en 0 para luego inicializar en 1 el array en donde guardaríamos resultados y usar tipo float en vez de double para todo el proyecto. Ninguno de los cambios tuvo verificaciones de correctitud.

La **métrica** que usamos para comparar resultados es el tiempo promedio que tarda en correr el programa dividido el tamaño del problema convertido a ms, es decir, el tiempo que tarda computar el magnetismo y la energia para una celda.

**No** probamos diferentes compiladores, funciones de generación de números aleatorios o cambios estructurales en el código.

## Observaciones

### Algunos resultados
Los gráficos de [Métrica en funcion del conjunto de banderas de optimiación](#Graficos) muestran diferentes compilaciones para el código en su versión 0. Se puede ver que

1. El salto más representativo de performance viene dado por la bandera de compilación `-O1`.

2. El tiempo que toma hacer los cálculos para una celda disminuye a medida que el tamaño del problema se agranda.

2. En la máquina zx81 la variación de la métrica en función del tamaño para todos los conjuntos de banderas de compilación es mucho mayor que en las otras PC.

3. Algunos conjuntos de banderas de compilación en atom para L chicos actúan en detrimento de la optimización. En particular para los conjuntos de bandera
  - "-O3 -floop-block"
  - "-O3 -floop-block -march=native"
  - "-O3 -floop-block -ffast-math"
  
pero no para
  - "-O3 -floop-block -ffast-math -march=native"

### Algunas estadísticas
De obtener estadísticas para diferentes conjuntos de banderas con L fijo en COMPLETAR en atom vimos que

Atom | Branch-misses | Cache-misses
---|---|---
-O0 | 1.16\% | 0.11\%
-03 -ffast-math | 1.15\% | 0.2\%
-03 -ffast-math -floop-back | 1.16\% | 0.2\%

empeoró la cantidad de caché misses y que prácticamente no mejoró la la branch-prediction.

En zx81 ocurrió que

zx81 | Branch-misses | Cache-misses
---|---|---
-O0 | 1.18\% | 0.10\%
-03 -ffast-math | 1.18\% | 0.21\%
-03 -ffast-math -floop-back | 1.18\% | 0.21\%

las tendencias son las mismas.

### Algunos cálculos hechos a pedal
Para atom, fijado el L, el tiempo de ejecución sin optimización y el tiempo de ejecución con optimizaciones varia entre un 20% y 30%.

### Modificaciones al código
Ninguna de las modificaciones en el cogido generaron cambios significativos de performance.

## Hipótesis

- La amplitud de la métrica que se obtiene de variar el L en zx81 se puede explicar por una cache pequeña en comparación con atom pero los resultados para la máquina d son contradictorios con esto.

- A L más grande, los cálculos se paralelizan en diferentes niveles y la memoria tiende a estabilizarse siguiendo patrones que el compilador puede reconocer y por lo tanto predecir mejor ramas, cargar en cache los datos que se sabe van a usar más frecuentemente, hacer tiling para computar operaciones sobre matrices grandes más rápido, etc.

- La documentación de O1 está incompleta e internamente hace magia negra.

## Conclusiones
Solo sé que no se nada.

## Gráficos
Acá irian los gráficos. SI TAN SOLO TUVIERAMOS ALGUNOS
