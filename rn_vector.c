/// Esto es un standalone para pruebas sobre el vector de aleatorios

#include "wtime.h"
#include <stdio.h>
#include <stdlib.h>
#include <mt19937.h>
#include <omp.h>

#define VECTOR_SIZE 64
#define N_HILOS 3

////////////////////////////////////////////////////////////////////
// ./a.out | uniq -c | cut -d' ' -f1 | uniq -c
////////////////////////////////////////////////////////////////////

void generate_random_array(float *rng_array, mt19937_state *state) {
    for (int i = 0; i < VECTOR_SIZE; i++) {
        rng_array[i] = mt19937_avx_generate_uniform_float_(state);
//        printf("%f\n", rng_array[i]);
    }
}

void init_mt19937_state_array(mt19937_state *state_array, int size) {
    for (int i = 0; i < size; i++) {
        mt19937_init_(&state_array[i]);
    }
}

int main(){
    // main
    mt19937_state n_aleatorio_red[N_HILOS];
    init_mt19937_state_array(n_aleatorio_red, N_HILOS);

    mt19937_state n_aleatorio_black[N_HILOS];
    init_mt19937_state_array(n_aleatorio_black, N_HILOS);

    for (int i = 0; i < N_HILOS; i++) {
        mt19937_skipahead_(&n_aleatorio_black[i], 1, 5);
    }

    float rn_vector[VECTOR_SIZE];
    int tid = 0;
    
    #pragma omp parallel default(none) shared(n_aleatorio_black,n_aleatorio_red) private(rn_vector,tid) num_threads(N_HILOS)
    {
    tid = omp_get_thread_num();
    generate_random_array(rn_vector, &n_aleatorio_red[tid]);
    for (int i = 0; i < VECTOR_SIZE; ++i)
    {
        printf("%f %d\n", rn_vector[i], tid );
        }

    generate_random_array(rn_vector, &n_aleatorio_black[tid]);
    for (int i = 0; i < VECTOR_SIZE; ++i)
    {
        printf("%f %d\n", rn_vector[i], tid );
        }
    }
    

    return 0;
}
