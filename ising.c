#include "ising.h"
#include <stdio.h>

size_t idx(size_t x, size_t y) {
    return y * WIDTH + x;
}

static int get_side_shift(grid_color color) {
    return color == RED ? -1 : 1;
}

void generate_random_array(float *rng_array, mt19937_state *state) {
    for (int i = 0; i < VECTOR_SIZE; i++) {
        rng_array[i] = mt19937_avx_generate_uniform_float_(state);
    }
}

void update_rb(grid_color color, const short * restrict read,
    short * restrict write, mt19937_state *n_aleatorio, float *bltz) {
    int side_shift = get_side_shift(color);
    float rn_vector[VECTOR_SIZE]; // Array de numeros aleatorios
    int tid = 0;

//    #pragma omp parallel default(none) private(tid,rn_vector,side_shift) shared(write,read,n_aleatorio,bltz) num_threads(N_HILOS) //¿definir variables aquí?
    {
//        #pragma omp for collapse(2)
        #pragma omp parallel default(none) firstprivate(side_shift) private(rn_vector, tid) shared(read, write, n_aleatorio, bltz)
        {  
            tid = omp_get_thread_num();
            #pragma omp for collapse(2)
            for (unsigned int j = 0; j < HEIGHT; j+=2) {  // no saltar en pares e impares, collapsar y calcular sideshift en private
                for (unsigned int i = 0; i < WIDTH; i+= VECTOR_SIZE) {
                    generate_random_array(rn_vector, &n_aleatorio[tid]);
                    #pragma omp simd
                    for (int k = 0; k < VECTOR_SIZE; k++) {
                        int vector_i = i + k;

                        int spin_old = write[idx(vector_i,j)];
                        int spin_new = -spin_old;

                        // computing h_before
                        int spin_neigh_up = read[idx(vector_i,(j - 1 + HEIGHT) % HEIGHT)];
                        int spin_neigh_center = read[idx(vector_i,j)];
                        // int spin_neigh_side = read[idx((vector_i + side_shift + WIDTH) % WIDTH,j)];
                        int spin_neigh_side = read[idx((vector_i + side_shift + WIDTH) % WIDTH,j)];
                        int spin_neigh_down = read[idx(vector_i,(j + 1) % HEIGHT)];
                        
                        int sum_neigh = spin_neigh_up + spin_neigh_center +
                            spin_neigh_side + spin_neigh_down;

                        int h_before = spin_old * -sum_neigh;
                        int h_after = -h_before;

                        int delta_E = h_after - h_before;
                        
                        int spin_flip = delta_E <= 0 || rn_vector[k] <= bltz[delta_E / 4 - 1];
                        write[idx(vector_i,j)] = spin_flip * spin_new + (1 - spin_flip) * write[idx(vector_i,j)];
                    }
                }
            }
//            tid = omp_get_thread_num();
            #pragma omp for collapse(2)
            for (unsigned int j = 1; j < HEIGHT; j+=2) {
                for (unsigned int i = 0; i < WIDTH; i+= VECTOR_SIZE) {
                    generate_random_array(rn_vector, &n_aleatorio[tid]);
                    #pragma omp simd
                    for (int k = 0; k < VECTOR_SIZE; k++) {
                        int vector_i = i + k;

                        int spin_old = write[idx(vector_i,j)];
                        int spin_new = -spin_old;

                        // computing h_before
                        int spin_neigh_up = read[idx(vector_i,(j - 1 + HEIGHT) % HEIGHT)];
                        int spin_neigh_center = read[idx(vector_i,j)];
                        // int spin_neigh_side = read[idx((vector_i + side_shift + WIDTH) % WIDTH,j)];
                        int spin_neigh_side = read[idx((vector_i - side_shift + WIDTH) % WIDTH,j)];
                        int spin_neigh_down = read[idx(vector_i,(j + 1) % HEIGHT)];
                        
                        int sum_neigh = spin_neigh_up + spin_neigh_center +
                            spin_neigh_side + spin_neigh_down;

                        int h_before = spin_old * -sum_neigh;
                        int h_after = -h_before;

                        int delta_E = h_after - h_before;
                        
                        int spin_flip = delta_E <= 0 || rn_vector[k] <= bltz[delta_E / 4 - 1];
                        write[idx(vector_i,j)] = spin_flip * spin_new + (1 - spin_flip) * write[idx(vector_i,j)];
                    }
                }
            }
        }
    }
}

void update(short * restrict grid_r, short * restrict grid_b,
    mt19937_state *n_aleatorio_red, mt19937_state *n_aleatorio_black,
    float *bltz) {
    update_rb(RED, grid_b, grid_r, n_aleatorio_red, bltz); // pintura offset    
    update_rb(BLACK, grid_r, grid_b, n_aleatorio_black, bltz);
}

float calculate_rb(const short * restrict neigh, const short * restrict grid, int *M_max) {
    int side_shift = get_side_shift(RED);

    int E = 0;
    int M = 0;

//    #pragma omp parallel default(none) private(side_shift) shared(neigh, grid, M_max, E, M) num_threads(N_HILOS) // probar E,M como private o lastprivate
    {
//        #pragma omp for reduction(+:E,M) collapse(2)
        #pragma omp parallel for default(none) private(side_shift) shared(neigh, grid, M_max) reduction(+:E,M) collapse(2)
        for (unsigned int j = 0; j < HEIGHT; j++) {
            side_shift = j % 2 == 0 ? -1 : 1; 
            for (unsigned int i = 0; i < WIDTH; ++i) {
                int spin = grid[idx(i,j)];

                int spin_neigh_up = neigh[idx(i,(j - 1 + HEIGHT) % HEIGHT)];
                int spin_neigh_center = neigh[idx(i,j)];
                // int spin_neigh_side = neigh[idx((i + side_shift + WIDTH) % WIDTH,j)];
                int spin_neigh_side = neigh[idx((i + side_shift + WIDTH) % WIDTH,j)];
                int spin_neigh_down = neigh[idx(i,(j + 1) % HEIGHT)];

                int sum_neigh = spin_neigh_up + spin_neigh_center +
                spin_neigh_side + spin_neigh_down;

                E += spin * sum_neigh;
                M += spin;
            }
        }
    }
    
    *M_max = M;
    return E;
}

float calculate(const short * restrict grid_r, const short * restrict grid_b, int* M_max) {
    int E = 0;
    E += calculate_rb(grid_b, grid_r, M_max);
    // E += calculate_rb(RED, grid_r, grid_b, M_max);
    // F += calculate_rb(BLACK, grid_r, grid_b, M_max);

    return - (double) E;
}
