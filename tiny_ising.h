#ifndef TINY_ISING_H
#define TINY_ISING_H

#include "ising.h"
#include "params.h"
#include "wtime.h"

#include <assert.h>
#include <limits.h> // UINT_MAX
#include <math.h> // expf()
#include <stdio.h> // printf()
// #include <stdlib.h> // rand()
#include <time.h> // time()
// #include <mt19937.h>
#include <omp.h>

// temperature, E, E^2, E^4, M, M^2, M^4
struct statpoint {
    float t[NPOINTS];
    float e[NPOINTS];
    float e2[NPOINTS];
    float e4[NPOINTS];
    float m[NPOINTS];
    float m2[NPOINTS];
    float m4[NPOINTS];
};

#define N_DELTA_E 2

#endif
