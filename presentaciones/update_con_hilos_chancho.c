

update_rb(color, read, write, estado_rng[N_HILOS]) {
    side_shift = get_side_shift(color);
    rn_vector[VECTOR_SIZE];
    tid = 0;

    #pragma omp parallel [...]
    {   
        tid = omp_get_thread_num();
        #pragma omp for collapse(2)
        for (j = 0; j < HEIGHT; j+=2) {
            for (i = 0; i < WIDTH; i+= VECTOR_SIZE) {
                side_shift = 1;
                [...]
            }
        }
        #pragma omp for collapse(2)
        for (j = 1; j < HEIGHT; j+=2) {
            for (i = 0; i < WIDTH; i+= VECTOR_SIZE) {
                side_shift = -1;
                [...]
            }
        }
    }
}

