set terminal pngcairo size 1000,700 font 'Latin-Modern-Math,18'
set output 'zx81_L_cores.png'
set title 'ZX81'
set grid
set multiplot layout 1,1
set yrange [10:1000]
set logscale y
set ylabel "Celdas por milisegundo (métrica)"
#set ytics 100
#set ytics format "%.f"
set ytics ('10' 10, '30' 30, '100' 100, '200' 200, '400' 400, '800' 800)
set xrange [0.5:6.5]
set xlabel "Tamaño de grilla (L)"
set xtics 1
set xtics ('512' 1, '1024' 2, '4096' 3, '8192' 4, '16384' 5, '32768' 6)
set key at screen 0.90,0.45-0.05*4 opaque
plot '-' \
w lp pointtype 7 linecolor "black" ps 1.5 title "Lab 2" 
1 33.180955657444
2 33.310934928397
3 33.128751855164
e
set key at screen 0.90,0.45-0.05*5 opaque
plot '-' \
w lp pointtype 7 dashtype 5 linecolor "grey70" ps 1.5 title "1 c" 
1 31.659875182176
2 27.076819220946
3 25.973243054576
e
set key at screen 0.90,0.45-0.05*3 opaque
plot '-' \
w lp pointtype 5 dashtype 5 linecolor "grey60" ps 1.5 title "4 c" 
2 109.385320959274
3 108.657953695915
4 106.617136758730
5 96.605661491982
e
set key at screen 0.90,0.45-0.05*2 opaque
plot '-' \
w lp pointtype 9 dashtype 5 linecolor "red" ps 1.5 title "8 c" 
2 213.392898571134
3 212.817419573489
4 209.809001014781
5 215.356092975266
e
set key at screen 0.90,0.45-0.05*1 opaque
plot '-' \
w lp pointtype 11 dashtype 5 linecolor "coral" ps 1.5 title "14 c" 
2 342.562130010302
3 376.372898206967
4 373.682168953728
5 374.743903388716
e
set key at screen 0.90,0.45 opaque
plot '-' \
w lp pointtype 7 dashtype 5 linecolor "dark-orange" ps 1.5 title "28 c" 
2 381.574634858799
3 632.868544141192
4 608.614705635354
5 634.063337286643
e