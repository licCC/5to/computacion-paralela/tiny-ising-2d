set terminal pngcairo size 1000,600 font 'Latin-Modern-Math,18'
set output 'core_scaling.png'
set title 'ZX81'
set grid
set multiplot layout 1,1
set yrange [0:1000]
set ylabel "Celdas por milisegundo (métrica)"
set ytics 100
set ytics format "%.f"
set mytics 3
set xrange [0:33]
set xtics ('1' 1, '4' 4, '8' 8, '14' 14, '28' 28)
set xlabel "Cantidad de hilos"
set key at screen 0.8,0.85 opaque
plot 27.076819220946*x linecolor "green" title "scaling ideal"
set key at screen 0.35,0.80-0.05*1 opaque
plot '-' \
w lp pointtype 7 dashtype 5 linecolor "blue" ps 1.5 title "L = 1024" 
1 31.303538866955
4 108.765033873833
8 213.392898571134
14 342.562130010302
28 381.574634858799
e
set key at screen 0.35,0.80-0.05*2 opaque
plot '-' \
w lp pointtype 5 dashtype 7 linecolor "dark-green" ps 1.5 title "L = 4096" 
1 27.076819220946
4 108.657953695915
8 212.817419573489
14 376.372898206967
28 632.868544141192
e
set key at screen 0.35,0.80-0.05*3 opaque
plot '-' \
w lp pointtype 9 dashtype 7 linecolor "red" ps 1.5 title "L = 8192" 
1 25.973243054576
4 106.617136758730
8 209.809001014781
14 373.682168953728
28 608.614705635354
e
set key at screen 0.35,0.80 opaque
plot '-' \
w p pointtype 4 linecolor "black" ps 2 title "Lab 2" 
1 33.310934928397
e
