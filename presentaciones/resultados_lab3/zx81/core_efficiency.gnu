set terminal pngcairo size 1000,600 font 'Latin-Modern-Math,18'
set output 'zx81_core_efficiency.png'
set title 'ZX81 - eficiencia'
set grid
set multiplot layout 1,1
set yrange [0:100]
set ylabel "Eficiencia"
set ytics 20
set ytics format "%.f"
set mytics 1
set xrange [0:33]
set xtics ('1' 1, '4' 4, '8' 8, '14' 14, '28' 28)
set xlabel "Cantidad de hilos"
set key at screen 0.35,0.40-0.05*1 opaque
plot '-' \
w lp pointtype 7 dashtype 5 linecolor "blue" ps 1.5 title "L = 1024" 
1 95.0434902239459
4 82.0941540626235
8 80.0761443013493
14 73.4555293172071
28 40.9104864812847
e
set key at screen 0.35,0.40-0.05*2 opaque
plot '-' \
w lp pointtype 5 dashtype 7 linecolor "dark-green" ps 1.5 title "L = 4096" 
1 81.2850773451677
4 81.5482618016269
8 79.8601945693461
14 80.7055656082376
28 67.8529379425324
e
set key at screen 0.35,0.40-0.05*3 opaque
plot '-' \
w lp pointtype 9 dashtype 7 linecolor "red" ps 1.5 title "L = 8192" 
1 77.9721226990668
4 80.016619908678
8 78.7312790326107
14 80.1285930703216
28 65.2525650621927
e
set key at screen 0.35,0.40 opaque
plot '-' \
w p pointtype 4 linecolor "black" ps 2 title "Lab 2" 
1 100
e
