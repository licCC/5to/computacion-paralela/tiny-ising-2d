set terminal pngcairo size 1000,600 font 'Latin-Modern-Math,18'
set output 'atom_core_efficiency.png'
set title 'atom - eficiencia'
set grid
set multiplot layout 1,1
set yrange [0:100]
set ylabel "Eficiencia"
set ytics 20
set ytics format "%.f"
set mytics 1
set xrange [0:33]
set xtics ('1' 1, '4' 4, '8' 8, '16' 16, '32' 32)
set xlabel "Cantidad de hilos"
set key at screen 0.35,0.40-0.05*1 opaque
plot '-' \
w lp pointtype 7 dashtype 5 linecolor "blue" ps 1.5 title "L = 1024" 
1 94.4021549380865
4 93.4029973352344
8 85.9529381599416
16 84.6681908565453
32 75.8757213900109
e
set key at screen 0.35,0.40-0.05*2 opaque
plot '-' \
w lp pointtype 5 dashtype 7 linecolor "dark-green" ps 1.5 title "L = 4096" 
1 95.249186943432
4 94.1131015710474
8 87.6323672450489
16 87.3052209476146
32 83.0744531342408
e
set key at screen 0.35,0.40-0.05*3 opaque
plot '-' \
w lp pointtype 9 dashtype 7 linecolor "red" ps 1.5 title "L = 8192" 
1 93.3909847879792
4 91.8655205786776
8 88.7072615216377
16 87.1478761935369
32 82.3703923613106
e
set key at screen 0.35,0.40 opaque
plot '-' \
w p pointtype 4 linecolor "black" ps 2 title "Lab 2" 
1 100
e
