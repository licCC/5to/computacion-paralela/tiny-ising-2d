set terminal pngcairo size 1000,700 font 'Latin-Modern-Math,18'
set output 'atom_L_cores.png'
set title 'atom'
set grid
set multiplot layout 1,1
set yrange [10:1000]
set logscale y
set ylabel "Celdas por milisegundo (métrica)"
#set ytics 100
#set ytics format "%.f"
set ytics ('10' 10, '30' 30, '100' 100, '200' 200, '400' 400, '800' 800)
set xrange [0.5:6.5]
set xlabel "Tamaño de grilla (L)"
set xtics 1
set xtics ('512' 1, '1024' 2, '4096' 3, '8192' 4, '16384' 5, '32768' 6)
set key at screen 0.90,0.45-0.05*4 opaque
plot '-' \
w lp pointtype 7 linecolor "black" ps 1.5 title "Lab 2" 
1 34.527247416011
2 34.313203997583
3 34.419960138252
e
set key at screen 0.90,0.45-0.05*5 opaque
plot '-' \
w lp pointtype 7 dashtype 5 linecolor "grey70" ps 1.5 title "1 c" 
1 32.378458809371
2 32.39240400202
e
set key at screen 0.90,0.45-0.05*3 opaque
plot '-' \
w lp pointtype 5 dashtype 5 linecolor "grey60" ps 1.5 title "4 c" 
2 128.198244061984
3 129.172882122104
4 126.088013918413
5 129.895400485597
e
set key at screen 0.90,0.45-0.05*2 opaque
plot '-' \
w lp pointtype 9 dashtype 5 linecolor "red" ps 1.5 title "8 c" 
2 235.945656101897
3 240.555783525638
4 243.506428852712
5 249.980500790436
e
set key at screen 0.90,0.45-0.05*1 opaque
plot '-' \
w lp pointtype 11 dashtype 5 linecolor "coral" ps 1.5 title "16 c" 
2 464.837904794709
3 479.315497028729
4 478.451656605590
5 485.385490554315
6 492.146962759188
e
set key at screen 0.90,0.45 opaque
plot '-' \
w lp pointtype 7 dashtype 5 linecolor "dark-orange" ps 1.5 title "32 c" 
2 833.132514086149
3 912.176210362513
4 904.445464465473
5 942.277842689478
6 899.674395490932
e