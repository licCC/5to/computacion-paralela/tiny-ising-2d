set terminal pngcairo size 1000,600 font 'Latin-Modern-Math,18'
set output 'atom_core_scaling.png'
set title 'atom'
set grid
set multiplot layout 1,1
set yrange [0:1000]
set ylabel "Celdas por milisegundo (métrica)"
set ytics 100
set ytics format "%.f"
set mytics 3
set xrange [0:33]
set xtics ('1' 1, '4' 4, '8' 8, '16' 16, '32' 32)
set xlabel "Cantidad de hilos"
set key at screen 0.8,0.85 opaque
plot 32.39240400202*x linecolor "green" title "scaling ideal"
set key at screen 0.35,0.80-0.05*1 opaque
plot '-' \
w lp pointtype 7 dashtype 5 linecolor "blue" ps 1.5 title "L = 1024" 
1 32.39240400202
4 126.088013918413
8 235.945656101897
16 464.837904794709
32 833.132514086149
e
set key at screen 0.35,0.80-0.05*2 opaque
plot '-' \
w lp pointtype 5 dashtype 7 linecolor "dark-green" ps 1.5 title "L = 4096" 
1 32.683047821939
4 129.172882122104
8 240.555783525638
16 479.315497028729
32 912.176210362513
e
set key at screen 0.35,0.80-0.05*3 opaque
plot '-' \
w lp pointtype 9 dashtype 7 linecolor "red" ps 1.5 title "L = 8192" 
1 32.045439125651
4 126.088013918413
8 243.506428852712
16 478.451656605590
32 904.445464465473
e
set key at screen 0.35,0.80 opaque
plot '-' \
w p pointtype 4 linecolor "black" ps 2 title "Lab 2" 
1 34.313203997583
e
#plot 34.313203997583 linecolor "black" dashtype 4 notitle 