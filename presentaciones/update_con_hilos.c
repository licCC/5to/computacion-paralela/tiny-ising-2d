

update_rb(color, read, write, estado_rng[N_HILOS]) {
    side_shift = get_side_shift(color);
    rn_vector[VECTOR_SIZE];
    tid = 0;

    #pragma [...] private(rn_vector, tid) [...]
    {   
        tid = omp_get_thread_num();
        #pragma omp for collapse(2)
        for (j = 0; j < HEIGHT; j++) {
            side_shift = j % 2 == 0 ? -1 : 1;
            for (i = 0; i < WIDTH; i+= VECTOR_SIZE) {
                generate_random_array(rn_vector, estado_rng[tid]);

                for (k = 0; k < VECTOR_SIZE; k++) {
                    vector_i = i + k;

                    old_spin = write[idx(vector_i,j)];
                    flip = get_spin(read, write, rn_vector[k]);
                    
                    if (flip)
                        write[idx(vector_i,j)] = - old_spin;
                }
            }
        }
    }
}