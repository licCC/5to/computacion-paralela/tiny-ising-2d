

calculate_rb(neigh, grid) {
    side_shift = get_side_shift(RED);

    #pragma omp parallel for default(none)
            private(side_shift)
            shared(neigh, grid)
            reduction(+:E,M) collapse(2)

    for (j = 0; j < HEIGHT; j++) {
        side_shift = j % 2 == 0 ? -1 : 1;
        for (i = 0; i < WIDTH; i++) {
            e = site_energy(neigh, grid);
            spin = grid[idx(i,j)];

            E += spin * e;
            M += spin;
        }
    }
}

