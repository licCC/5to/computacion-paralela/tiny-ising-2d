struct statpoint_aos {
  float t, e, e2, e4, m, m2, m4;};

struct statpoint_aos stats[NPOINTS];

struct statpoint_soa {
    float t[NPOINTS], e[NPOINTS], e2[NPOINTS], e4[NPOINTS];
    float m[NPOINTS], m2[NPOINTS], m4[NPOINTS]; };

struct statpoint_soa stats;