#!/bin/bash

# El script se ejecuta desde la carpeta raiz de la siguiente manera
# `bash runs/pam.sh`

L="1024"
n_points=`echo "scale=12; $L ^ 2" | bc`

cflags=" " #-O2 -ffast-math -march=native -ftree-vectorize" // Abajo uso la opción ptiny_ising del Makefile
# printf 'Cflags: <%s>\n' $cflags #debug

# Agrega las banderas de compilacion al makefile
sed -i -e "s/CFLAGS = /CFLAGS = $cflags -DL=$L /" Makefile

# Compila
make clean
make ptiny_ising >& /dev/null
# printf "j: <%d>\n" $j #debug

# Reestablece las banderas de compilacion
sed -i -e "s/CFLAGS = $cflags -DL=$L /CFLAGS = /" Makefile

total_s_time_array=()

export OMP_NUM_THREADS=4
export OMP_PROC_BIND=close

./tiny_ising > results/output.dat

total_s_time=`cat results/output.dat | grep "Total Simulation Time" | grep -Eo '[0-9]+\.[0-9]+'`
# LC_NUMERIC="en_US.UTF-8" printf "total_s_time: %f\n" $total_s_time #debug

# Limpio el output.dat, dejo solo lo que sirve para graficar
sed -i '1,/# Temp	E	E^2	E^4	M	M^2	M^4/d' results/output.dat

printf '%s\n' "${total_s_time_array[@]}" #debug

# celdas por milisegundo
metrica=`echo "scale=12; $n_points / ($total_s_time * 1000)" | bc`

printf 'Celdas por milisegundo %s\n' "${metrica}" #debug

## Grafico
echo "set terminal pngcairo size 1000,1000 enhanced font 'Latin-Modern-Math,18'"	 > plot.gnu
echo "set output '1024_correctitud.png'"						>> plot.gnu

echo "set multiplot layout 2,2 rowsfirst"						>> plot.gnu
echo "set xrange [ 1.5 : 3.0 ] noreverse nowriteback"					>> plot.gnu
echo "set xtics 0.4"									>> plot.gnu
echo "set mxtics 4"									>> plot.gnu
echo "set grid"										>> plot.gnu

echo "set macros"									>> plot.gnu
# Margins for each row resp. column
echo "TMARGIN = \"set tmargin at screen 0.90; set bmargin at screen 0.55\""		>> plot.gnu
echo "BMARGIN = \"set tmargin at screen 0.50; set bmargin at screen 0.15\""		>> plot.gnu
echo "LMARGIN = \"set lmargin at screen 0.10; set rmargin at screen 0.50\""		>> plot.gnu
echo "RMARGIN = \"set lmargin at screen 0.50; set rmargin at screen 0.90\""		>> plot.gnu
# Ytics
echo "NOYTICS = \"set format y ''; unset ylabel\""					>> plot.gnu
echo "YTICS = \"set format y '%.1f'\""							>> plot.gnu



# 1st Row - Energy

# --- GRAPH a - Actual E(T)
echo "@TMARGIN; @LMARGIN; @YTICS"							>> plot.gnu
echo "set yrange [ -2 : 0 ]; set ylabel 'Energía'"					>> plot.gnu
echo "set label 1 'nuevo' at graph 0.80,0.9"						>> plot.gnu
echo "plot 'output.dat' u 1:2 w lp lc 'dark-orange' notitle"				>> plot.gnu

# --- GRAPH b - Original E(T)
echo "@TMARGIN; @RMARGIN; @NOYTICS"							>> plot.gnu
echo "unset ylabel"									>> plot.gnu
echo "set label 1 'base' at graph 0.85,0.9"						>> plot.gnu
echo "plot '1024_fbase.dat' u 1:2 w lp lc 'dark-blue' notitle"				>> plot.gnu



# 2nd Row - Magnetization

# --- GRAPH c - Actual M(T)
echo "@BMARGIN; @LMARGIN; @YTICS"							>> plot.gnu
echo "set yrange [ 0 : 1 ]; set ylabel 'Magnetización'"					>> plot.gnu
echo "set label 1 'nuevo' at graph 0.80,0.9"						>> plot.gnu
echo "plot 'output.dat' u 1:5 w lp lc 'dark-orange' notitle"				>> plot.gnu

# --- GRAPH d - Original M(T)
echo "@BMARGIN; @RMARGIN; @NOYTICS"							>> plot.gnu
echo "set xlabel 'Temperatura'; set xlabel offset screen -0.20,0"			>> plot.gnu
echo "unset ylabel"									>> plot.gnu
echo "set label 1 'base' at graph 0.85,0.9"						>> plot.gnu
echo "plot '1024_fbase.dat' u 1:5 w lp lc 'dark-blue' notitle"				>> plot.gnu


echo "unset multiplot"									>> plot.gnu
### End multiplot

mv plot.gnu results/plot.gnu
cp runs/1024_fbase_pam.dat results/1024_fbase.dat
cd results
gnuplot plot.gnu
xdg-open 1024_correctitud.png
rm output.dat
rm 1024_fbase.dat
rm plot.gnu
cd ..



#done < "${file}"
