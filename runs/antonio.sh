#!/bin/bash

# El script se compila desde la carpeta raiz de la siguiente manera
# `bash runs/antonio.sh 0.0 5 runs/banderas.txt`
# donde
# - El primer argumento del script identifica la version del codigo
# - El segundo la cantidad de veces que se quiere ejecutar cada compilacion
# - El tercero el archivo con los conjuntos de banderas de compilacion
printf "Argumentos - host:%s, n_samples:%d, compilation_flags in:%s\n" $1 $2 $3

cabecera="Maquina,VCodigo,Banderas de compilacion,Numero de Sitios,Samples,Promedio[s],Stdev[s],ErrRel,Metrica[ms]"
# Si el archivo de resultados no existe agrega una cabecera
if [ ! -f results/results.csv ]; then
    printf "$cabecera\n" > results/results.csv
fi

file=$3
while read -d "\\" -r line; do
  for j in 1024 4096 8192 16384 #32768
  do
  cflags="${line}"
  # printf 'Cflags: <%s>\n' $cflags #debug

  # Agrega las banderas de compilacion al makefile
  sed -i -e "s/CFLAGS = /CFLAGS = $cflags -DL=$j /" Makefile

  L=$j # L=`cat params.h | grep -i "#define L" | grep -Eo '[0-9]*'`
  n_points=`echo "scale=12; $L ^ 2" | bc`

  # Compila
  make clean
  make ptiny_ising >& /dev/null
  # printf "j: <%d>\n" $j #debug

  # Reestablece las banderas de compilacion
  sed -i -e "s/CFLAGS = $cflags -DL=$j /CFLAGS = /" Makefile

  acum_s_time=0.0
  total_s_time_array=()
  for ((i = 1; i <= $2; i++))
  do
    total_s_time=`./tiny_ising | grep "Total Simulation Time" | grep -Eo '[0-9]+\.[0-9]+'`
    # LC_NUMERIC="en_US.UTF-8" printf "total_s_time: %f\n" $total_s_time #debug

    acum_s_time=`echo "scale=12; $acum_s_time + $total_s_time" | bc`
    total_s_time_array+=($total_s_time)
  done

  # printf '%s\n' "${total_s_time_array[@]}" #debug

  # Calculo de promedio, desviacion estandar y metrica
  promedio=`echo "scale=12; $acum_s_time / $2" | bc`
  # LC_NUMERIC="en_US.UTF-8" printf "acum: %f promedio: %f\n" $acum_s_time $promedio #debug
  # Metrica: celdas por milisegundo
  metrica=`echo "scale=12; $n_points / ($promedio * 1000) " | bc`

  sum_dif=0.0
  for ((i = 1; i <= $2; i++))
  do
    i_s_time=`echo "${total_s_time_array[$i - 1]}"`
    # LC_NUMERIC="en_US.UTF-8" printf "i_s_time: %f\n" $i_s_time #debug
    sum_dif=`echo "scale=12; $sum_dif + ($i_s_time - $promedio) ^ 2" | bc`
    # LC_NUMERIC="en_US.UTF-8" printf "sum_dif: %f\n" $sum_dif #debug
  done

  stdev=`echo "scale=12; sqrt($sum_dif / $2)" | bc`
  err_rel_porc=`echo "scale=12; 100 * ($stdev / $promedio)" | bc`
  
  host=`hostname`
  resultado="$host,$1,$cflags,$n_points,$2,$promedio,$stdev,$err_rel_porc,$metrica"
  # LC_NUMERIC="en_US.UTF-8" printf "resultado: %s\n" $resultado #debug
  printf "$resultado\n" >> results/results.csv
  done
done < "${file}"
