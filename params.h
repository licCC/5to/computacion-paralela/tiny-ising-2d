#pragma once

#ifndef L // linear system size
#define L 1024
#endif

#ifndef TEMP_INITIAL // initial temperature
#define TEMP_INITIAL 1.5
#endif

#ifndef TEMP_FINAL // final temperature
#define TEMP_FINAL 3.0
#endif

#ifndef TEMP_DELTA // temperature step
#define TEMP_DELTA 0.01
#endif

#ifndef TRAN // equilibration time
#define TRAN 5
#endif

#ifndef TMAX // measurement time
#define TMAX 25
#endif

#ifndef DELTA_T // sampling period for energy and magnetization
#define DELTA_T 5
#endif

// out vector size, it is +1 since we reach TEMP_
#define NPOINTS (1 + (int)((TEMP_FINAL - TEMP_INITIAL) / TEMP_DELTA))

#define N (L * L) // system size

// Ancho de la matriz shuffleada
#define WIDTH (L/2)

// Alto de la matriz shuffleada
#define HEIGHT (L)

// Tamano del vector en bytes
#define VECTOR_SIZE (L/2)

// Cantidad de hilos (equivale a la cantidad de estados del RNG que voy a generar, capaz no los use todos)
#define N_HILOS 8
