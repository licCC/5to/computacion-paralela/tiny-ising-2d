#ifndef ISING_H
#define ISING_H

#include "params.h"
#include <math.h>
#include <stdlib.h>
#include <float.h>
#include <mt19937.h>
#include <omp.h>

typedef enum {RED, BLACK} grid_color;

void update(short * restrict grid_r, short * restrict grid_b, mt19937_state *n_aleatorio_red, mt19937_state *n_aleatorio_black, float *bltz);

float calculate(const short * restrict grid_r, const short * restrict grid_b, int* M_max);

size_t idx(size_t x, size_t y);

#endif
