/*
 * Tiny Ising model.
 * Loosely based on  "q-state Potts model metastability
 * study using optimized GPU-based Monte Carlo algorithms",
 * Ezequiel E. Ferrero, Juan Pablo De Francesco, Nicolás Wolovick,
 * Sergio A. Cannas
 * http://arxiv.org/abs/1101.0876
 *
 * Debugging: Ezequiel Ferrero
 */

#include "tiny_ising.h"


static void precalcular_boltzman(float *bltz, float temp) {
  bltz[0] = expf(-4 / temp);
  bltz[1] = expf(-8 / temp);
}

static void cycle(short * restrict grid_r, short * restrict grid_b,
                  const float min, const float max,
                  const float step, const unsigned int calc_step,
                  struct statpoint *stats,
                  mt19937_state *n_aleatorio_red,
                  mt19937_state *n_aleatorio_black)
{

    float bltz[N_DELTA_E];

    assert((0 < step && min <= max) || (step < 0 && max <= min));
    int modifier = (0 < step) ? 1 : -1;

    unsigned int index = 0;
    for (float temp = min; modifier * temp <= modifier * max; temp += step) {

        precalcular_boltzman(bltz, temp);

        // equilibrium phase
        for (unsigned int j = 0; j < TRAN; ++j) {
            update(grid_r, grid_b, n_aleatorio_red, n_aleatorio_black, bltz);
        }

        // measurement phase
        unsigned int measurements = 0;
        float e = 0.0, e2 = 0.0, e4 = 0.0, m = 0.0, m2 = 0.0, m4 = 0.0;
        for (unsigned int j = 0; j < TMAX; ++j) {
            update(grid_r, grid_b, n_aleatorio_red, n_aleatorio_black, bltz);
            if (j % calc_step == 0) {
                float energy = 0.0, mag = 0.0;
                int M_max = 0;
                energy = calculate(grid_r, grid_b, &M_max);
                mag = abs(M_max) / (float)(L * L * 0.5);
                e += energy;
                e2 += energy * energy;
                e4 += energy * energy * energy * energy;
                m += mag;
                m2 += mag * mag;
                m4 += mag * mag * mag * mag;
                ++measurements;
            }
        }
        assert(index < NPOINTS);
        stats->t[index] = temp;
        stats->e[index] += e / measurements;
        stats->e2[index] += e2 / measurements;
        stats->e4[index] += e4 / measurements;
        stats->m[index] += m / measurements;
        stats->m2[index] += m2 / measurements;
        stats->m4[index] += m4 / measurements;
        ++index;
    }
}


static void init(short  *grid_r, short *grid_b) {
   for (unsigned int i = 0; i < HEIGHT; ++i) {
       for (unsigned int j = 0; j < WIDTH; ++j) {
           grid_r[i * WIDTH + j] = 1;
           grid_b[i * WIDTH + j] = 1;
       }
   }
}

void init_mt19937_state_array(mt19937_state *state_array, int size) {
    for (int i = 0; i < size; i++) {
        mt19937_init_(&state_array[i]);
    }
}

int main(void) {
    // parameter checking
    // static_assert(TEMP_DELTA != 0, "Invalid temperature step");
    // static_assert(((TEMP_DELTA > 0) && (TEMP_INITIAL <= TEMP_FINAL)) || ((TEMP_DELTA < 0) && (TEMP_INITIAL >= TEMP_FINAL)), "Invalid temperature range+step");
    // static_assert(TMAX % DELTA_T == 0, "Measurements must be equidistant"); // take equidistant calculate()
    // static_assert((L * L / 2) * 4ULL < UINT_MAX, "L too large for uint indices"); // max energy, that is all spins are the same, fits into a ulong

    // the stats
    struct statpoint stat;
    for (unsigned int i = 0; i < NPOINTS; ++i) {
        stat.t[i] = 0.0;
        stat.e[i] = stat.e2[i] = stat.e4[i] = 0.0;
        stat.m[i] = stat.m2[i] = stat.m4[i] = 0.0;
    }

    // print header
    printf("# L: %i\n", L);
    // printf("# Minimum Temperature: %f\n", TEMP_INITIAL);
    // printf("# Maximum Temperature: %f\n", TEMP_FINAL);
    // printf("# Temperature Step: %.12f\n", TEMP_DELTA);
    // printf("# Equilibration Time: %i\n", TRAN);
    // printf("# Measurement Time: %i\n", TMAX);
    // printf("# Data Acquiring Step: %i\n", DELTA_T);
    // printf("# Number of Points: %i\n", NPOINTS);

    // mt19937_state n_aleatorio_red;
    // mt19937_init_(&n_aleatorio_red);
    // mt19937_state n_aleatorio_black;
    // mt19937_init_(&n_aleatorio_black);
    // mt19937_skipahead_(&n_aleatorio_black, 1, 5); // jumps ahead N = 1*2^5 output values inside an RNG sequence

    mt19937_state n_aleatorio_red[N_HILOS];
    assert(NULL != n_aleatorio_red);
    init_mt19937_state_array(n_aleatorio_red, N_HILOS);

    mt19937_state n_aleatorio_black[N_HILOS];
    assert(NULL != n_aleatorio_black);
    init_mt19937_state_array(n_aleatorio_black, N_HILOS);

    for (int i = 0; i < N_HILOS; i++) {
        mt19937_skipahead_(&n_aleatorio_black[i], 1, 5);
        // jumps ahead N = 1*2^5 output values inside an RNG sequence (sizeof float)
    }

    // clear the grid
    size_t grid_size = (L * L * 0.5) * sizeof(int);
    short *grid_r = malloc(grid_size);
    assert(NULL != grid_r);
    short *grid_b = malloc(grid_size);
    assert(NULL != grid_b);
    init(grid_r, grid_b);

    // start timer
    double start = wtime();

    // temperature increasing cycle
    cycle(grid_r, grid_b, TEMP_INITIAL, TEMP_FINAL, TEMP_DELTA, DELTA_T,
        &stat, n_aleatorio_red, n_aleatorio_black);

    // stop timer
    double elapsed = wtime() - start;
    printf("# Total Simulation Time (sec): %lf\n", elapsed);

    printf("# Temp\tE\tE^2\tE^4\tM\tM^2\tM^4\n");
    for (unsigned int i = 0; i < NPOINTS; ++i) {
        printf("%lf\t%.10lf\t%.10lf\t%.10lf\t%.10lf\t%.10lf\t%.10lf\n",
               stat.t[i],
               stat.e[i] / ((float)N),
               stat.e2[i] / ((float)N * N),
               stat.e4[i] / ((float)N * N * N * N),
               stat.m[i],
               stat.m2[i],
               stat.m4[i]);
    }

    free(grid_r);
    free(grid_b);

    return 0;
}
