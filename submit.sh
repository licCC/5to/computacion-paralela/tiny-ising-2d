#!/bin/bash

### tiny-ising cp202305
#SBATCH --job-name=RNG64

#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --time 01:00:00
. /etc/profile

export OMP_NUM_THREADS=1
export OMP_PROC_BIND=close

srun bash runs/antonio.sh 3.0 2 runs/banderas.txt
