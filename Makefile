CC = clang
CFLAGS = -std=c11 -Wall -Wextra -Iinclude -Llib
DBGFLAGS = -g
LDFLAGS = -lm -lrngavx
GL_LDFLAGS = -lGL -lglfw
VECT_FLAGS = -ftree-vectorize -Rpass=loop-vectorize -Rpass-missed=loop-vectorize -fopenmp
PRLL_FLAGS = -ftree-vectorize -fopenmp

# Files
TARGETS = tiny_ising

# Rules
all: $(TARGETS)

tiny_ising: tiny_ising.c ising.c wtime.o
	$(CC) $(CFLAGS) $(VECT_FLAGS) -o $@ $^ $(LDFLAGS)

vtiny_ising: tiny_ising.c ising.c wtime.o
	$(CC) -O2 -ffast-math -march=native $(CFLAGS) $(VECT_FLAGS) -o tiny_ising $^ $(LDFLAGS)

ptiny_ising: tiny_ising.c ising.c wtime.o
	$(CC) -O2 -ffast-math -march=native $(DBGFLAGS) $(CFLAGS) $(PRLL_FLAGS) -o tiny_ising $^ $(LDFLAGS)
	
rn_vector: rn_vector.c wtime.o
	$(CC) -O2 -ffast-math -march=native $(CFLAGS) $^ -Iinclude -Llib -lrngavx -fopenmp

wtime: wtime.c wtime.h
	$(CC) $(CFLAGS) -o $@ wtime.c

clean:
	rm -f $(TARGETS) *.o

.PHONY: clean all

